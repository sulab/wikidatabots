# -*- coding: utf-8 -*-
#!usr/bin/env python
# -*- coding: utf-8 -*-

'''
Author:Andra Waagmeester (andra@waagmeester.net)

This file is part of ProteinBoxBot.

ProteinBoxBot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProteinBoxBot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProteinBoxBot.  If not, see <http://www.gnu.org/licenses/>.
'''

import xml.etree.ElementTree as ET
import sys
import requests
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../ProteinBoxBot_Core")
import copy
import pprint
import ProteinBoxBot_Core.PBB_Core as PBB_Core
import ProteinBoxBot_Core.PBB_Debug as PBB_Debug
import ProteinBoxBot_Core.PBB_login as PBB_login
import ProteinBoxBot_Core.PBB_settings as PBB_settings
from time import gmtime, strftime
import time
import traceback

def getPubmedObject(pmid):
      pubmedUrl = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=Pubmed&Retmode=xml&id='+str(pmid)
      r = requests.get(pubmedUrl)
      return ET.fromstring(r.text)

class pubmedEntry(object):
    def __init__(self, object):
        root = getPubmedObject(object)
        self.pmid =  root.findall(".//MedlineCitation/PMID")[0].text
        self.articleTitle = root.findall(".//MedlineCitation/Article/ArticleTitle")[0].text
        pmcset = root.findall(".//ArticleIdList/ArticleId[@IdType='pmc']")
        if len(pmcset) > 0:
            self.pmc = pmcset[0].text
        doiset = root.findall(".//ArticleIdList/ArticleId[@IdType='doi']")
        if len(doiset) > 0:
            self.doi = doiset[0].text

article = pubmedEntry(sys.argv[1])
print(vars(article))
print(article.pmid)
print(article.articleTitle)
if "pmc" in vars(article):
    print(article.pmc)
if "doi" in vars(article):
    print(article.doi)

prep = dict()
refStatedIn = PBB_Core.WDItemID(value='Q180686', prop_nr='P248', is_reference=True)
refPubmedId = PBB_Core.WDString(value=str(article.pmid), prop_nr='P698', is_reference=True)
timeStringNow = strftime("+%Y-%m-%dT00:00:00Z", gmtime())
refRetrieved = PBB_Core.WDTime(timeStringNow, prop_nr='P813', is_reference=True)
pubmed_reference = [refStatedIn, refPubmedId, refRetrieved]

# P31 instance of
prep['P31'] = [PBB_Core.WDItemID(value='Q13442814', prop_nr='P31', references=[copy.deepcopy(pubmed_reference)])]

# P698 PubMed ID
prep['P698'] = [
            PBB_Core.WDExternalID(value=str(article.pmid), prop_nr='P698', references=[copy.deepcopy(pubmed_reference)])]

# P1476 title
prep['P1476'] = [PBB_Core.WDMonolingualText(value=str(article.articleTitle), prop_nr='P1476', references=[copy.deepcopy(pubmed_reference)])]

if 'doi' in vars(article):
    prep['P356'] = [PBB_Core.WDExternalID(value=str(article.doi), prop_nr='P356', references=[copy.deepcopy(pubmed_reference)])]

if 'pmc' in vars(article):
    prep['P932'] = [PBB_Core.WDExternalID(value=str(article.pmc[3:]), prop_nr='P932', references=[copy.deepcopy(pubmed_reference)])]

data2add = []
for key in prep.keys():
        for statement in prep[key]:
            data2add.append(statement)

wdPage = PBB_Core.WDItemEngine(item_name=article.articleTitle, data=data2add, server="www.wikidata.org", domain="scientific_article")
if wdPage.get_description() == "":
        wdPage.set_description(description='scientific article', lang='en')

test = wdPage.get_wd_json_representation()

logincreds = PBB_login.WDLogin(PBB_settings.getWikiDataUser(), os.environ['wikidataApi'])
wdPage.write(logincreds)


