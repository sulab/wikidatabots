import sys
import os

sys.path.append('../../ProteinBoxBot_Core/')
sys.path.append('../../obo/')
import obo_importer

"""
A OBO bot controller for simple invoking of Gene Ontolgoy and other bots on jenkins
"""

__author__ = 'Sebastian Burgstaller-Muehlbacher'
__license__ = 'AGPLv3'
__copyright__ = 'Sebastian Burgstaller-Muehlbacher'


def main():
    obo_importer.run(user='ProteinBoxBot', pwd=os.environ['wikidataApi'])


if __name__ == '__main__':
    sys.exit(main())
