import sys
import os
import json
import pandas as pd
import pprint
import time
import numpy as np

import PBB_Core
import PBB_login

"""
A bot for import of human microRNAs. It creates new Wikidata items and links the miRNAs to the genes they are being
encoded by. Furthermore, it establishes links to
"""

__author__ = 'Sebastian Burgstaller-Muehlbacher'
__license__ = 'AGPLv3'
__copyright__ = 'Sebastian Burgstaller-Muehlbacher'


# mirtar_base = pd.read_csv('~/Downloads/hsa_MTI.csv', header=0, sep=',')
mirtar_base = pd.read_csv('./data/hsa_MTI.csv', low_memory=False,
                          dtype={'References (PMID)': np.str, 'Target Gene (Entrez Gene ID)': np.str})

mirbase_data = pd.read_csv('./mirbase_data/hsa.gff3', sep='\t', low_memory=False, skiprows=13, index_col=None,
                           names=['chr', 'ph', 'type', 'start', 'stop', 'ph2', 'orientation', 'ph3', 'info'],
                           dtype={'start': np.str, 'stop': np.str})

enc_map = pd.read_csv('./mirbase_data/mirna_context.txt', sep='\t', low_memory=False, skiprows=13, index_col=None,
                           names=['id', 'transcript', 'orientation', 'part', 'chr', 'orientation', 'hgnc', 'clone'])

print(enc_map.head())

chromosome_map = {
    'chr1': 'Q430258',
    'chr2': 'Q638893',
    'chr3': 'Q668633',
    'chr4': 'Q836605',
    'chr5': 'Q840741',
    'chr6': 'Q540857',
    'chr7': 'Q657319',
    'chr8': 'Q572848',
    'chr9': 'Q840604',
    'chr10': 'Q840737',
    'chr11': 'Q847096',
    'chr12': 'Q847102',
    'chr13': 'Q840734',
    'chr14': 'Q138955',
    'chr15': 'Q765245',
    'chr16': 'Q742870',
    'chr17': 'Q220677',
    'chr18': 'Q780468',
    'chr19': 'Q510786',
    'chr20': 'Q666752',
    'chr21': 'Q753218',
    'chr22': 'Q753805',
    'chrX': 'Q61333',
    'chrY': 'Q202771',
    'chrMT': 'Q27075'
}


def get_entrez_qid_map(prop_nr):
    query = '''
        SELECT * WHERE {{
            ?qid wdt:{} ?id .
            ?qid wdt:P703 wd:Q15978631 .

            OPTIONAL {{
                ?qid wdt:P353 ?id .
            }}
        }}
        '''.format(prop_nr)

    results = PBB_Core.WDItemEngine.execute_sparql_query(query=query)['results']['bindings']

    id_wd_map = dict()
    hgnc_map = dict()
    for z in results:
        id_wd_map[z['id']['value']] = z['qid']['value'].split('/')[-1]
        if 'hgnc' in z:
            hgnc_map[z['hgnc']['value']] = z['qid']['value'].split('/')[-1]

    return id_wd_map, hgnc_map


def make_refs(mirna_label, mirna_id, prop_nr, database):
    refs = [[
        PBB_Core.WDItemID(value=database, prop_nr='P248', is_reference=True),  # stated in
        PBB_Core.WDExternalID(value=mirna_id, prop_nr=prop_nr, is_reference=True),  # source element
        PBB_Core.WDItemID(value='Q1860', prop_nr='P407', is_reference=True),  # language of work
        PBB_Core.WDMonolingualText(value=mirna_label, language='en', prop_nr='P1476', is_reference=True),
        PBB_Core.WDTime(time=time.strftime('+%Y-%m-%dT00:00:00Z'), prop_nr='P813', is_reference=True)  # retrieved
    ]]

    return refs


def get_gene(chromosome, start, stop):
    query = '''
            SELECT DISTINCT ?gene ?geneLabel ?start ?stop WHERE {{
              ?gene wdt:P1057 wd:{} .
              ?gene wdt:P279 wd:Q7187 .

              ?gene p:P644 ?statement .
              ?gene wdt:P703 wd:Q15978631 .
              ?gene wdt:P2548 ?strand_orientation .

              ?statement v:P644 ?start .
              ?statement q:P659 wd:Q20966585 .

              ?gene p:P645 ?statement2 .
              ?statement2 v:P645 ?stop .
              ?statement2 q:P659 wd:Q20966585 .

              SERVICE wikibase:label {{
                    bd:serviceParam wikibase:language "en" .
              }}

              FILTER (xsd:integer(?start) >= {} && xsd:integer(?stop) <= {})
            }}
            ORDER BY ?geneLabel
            '''.format(chromosome, start, stop)

    results = PBB_Core.WDItemEngine.execute_sparql_query(query=query)['results']['bindings']

    gene_qids = []
    for x in results:
        if x['geneLabel']['value'].startswith('MIR'):
            return [x['gene']['value'].split('/')[-1]]
        else:
            gene_qids.append(x['gene']['value'].split('/')[-1])

    return gene_qids


def generate_targets(mirmat_name):
    data = []
    targets = mirtar_base.loc[mirtar_base['miRNA'].values == mirmat_name, :]
    targets = targets[['miRTarBase ID', 'miRNA', 'Target Gene (Entrez Gene ID)']].drop_duplicates()
    print(targets.count())
    print(len(targets['miRTarBase ID'].unique()))
    for count, mir in targets.iterrows():

        acc = mir['miRTarBase ID']
        ncbi_id = mir['Target Gene (Entrez Gene ID)']
        mirna_label = mir['miRNA']

        if ncbi_id not in entrez_qid_map:
            continue

        # print(acc, ncbi_id)

        refs = [[
            PBB_Core.WDItemID(value='Q6826951', prop_nr='P248', is_reference=True),  # stated in
            PBB_Core.WDExternalID(value=acc, prop_nr='P2646', is_reference=True),  # source element
            PBB_Core.WDItemID(value='Q1860', prop_nr='P407', is_reference=True),  # language of work
            PBB_Core.WDMonolingualText(value=mirna_label, language='en', prop_nr='P1476', is_reference=True),
            PBB_Core.WDTime(time=time.strftime('+%Y-%m-%dT00:00:00Z'), prop_nr='P813', is_reference=True)  # retrieved
        ]]

        stmnt = PBB_Core.WDItemID(value=entrez_qid_map[ncbi_id], prop_nr='P128', references=refs)
        data.append(stmnt)

    return data


def get_rnas(pre_mirna=True):
    if pre_mirna:
        prop_nr = 'P2870'
        class_qid = 'Q310899'
    else:
        prop_nr = 'P2871'
        class_qid = 'Q23838648'

    query = '''
           SELECT ?mirna ?mirna_id ?mirnaLabel WHERE {{
              ?mirna wdt:P279 wd:{} .
              ?mirna wdt:{} ?mirna_id .
              SERVICE wikibase:label {{bd:serviceParam wikibase:language "en" .}}
            }}
            '''.format(class_qid, prop_nr)

    results = PBB_Core.WDItemEngine.execute_sparql_query(query=query)['results']['bindings']

    mirna_id_qid_map = {}
    for x in results:
        if 'mirna_id' in x:
            mirna_id_qid_map.update({x['mirna']['value']: x['mirna_id']['value']})
    return mirna_id_qid_map

entrez_qid_map, hgnc_qid_map = get_entrez_qid_map('P351')


def main():
    login_obj = PBB_login.WDLogin(user='ProteinBoxBot', pwd=sys.argv[1])

    progress = {}
    progress_file_name = 'mirnabot_progress.json'
    if os.path.isfile(progress_file_name):
        with open(progress_file_name, 'r') as infile:
            progress = json.load(infile)

    premir_map = {}
    matmir_map = {}

    for count, premir in mirbase_data.iterrows():
        start_time = time.time()
        chromosome = premir['chr']
        start = premir['start']
        stop = premir['stop']
        orientation = premir['orientation']

        if premir['type'] == 'miRNA_primary_transcript':
            print(premir['info'])

            mirna_id, alias, name = [x.split('=')[1] for x in premir['info'].split(';')]

            if mirna_id in progress:
                premir_map.update({mirna_id: progress[mirna_id]})
                continue

            genome_build_qualifier = [PBB_Core.WDItemID(value='Q20966585', prop_nr='P659', is_qualifier=True)]
            strand_orientation = 'Q22809680' if orientation == '+' else 'Q22809711'

            refs = make_refs(mirna_label=name, mirna_id=mirna_id, prop_nr='P2870', database='Q6826947')
            data = [
                PBB_Core.WDItemID(value='Q310899', prop_nr='P31', references=refs),
                PBB_Core.WDItemID(value='Q15978631', prop_nr='P703', references=refs),
                PBB_Core.WDExternalID(value=mirna_id, prop_nr='P2870', references=refs),
                PBB_Core.WDItemID(value=chromosome_map[chromosome], prop_nr='P1057', references=refs),  # chromosome
                PBB_Core.WDString(value=start, prop_nr='P644', references=refs, qualifiers=genome_build_qualifier),
                PBB_Core.WDString(value=stop, prop_nr='P645', references=refs, qualifiers=genome_build_qualifier),
                PBB_Core.WDItemID(value=strand_orientation, prop_nr='P2548', references=refs,
                                  qualifiers=genome_build_qualifier),
                PBB_Core.WDBaseDataType.delete_statement('P279')
            ]

            gene_qids = set(get_gene(chromosome=chromosome_map[chromosome], start=start, stop=stop))
            print(gene_qids)

            for gene in gene_qids:
                data.append(PBB_Core.WDItemID(value=gene, prop_nr='P702', references=refs))

            try:
                wd_item = PBB_Core.WDItemEngine(item_name='miRNA', domain='microRNAs', data=data)

                wd_item.set_label(name)
                wd_item.set_description('human precursor microRNA')
                if alias != name:
                    wd_item.set_aliases([alias], append=True)

                qid = wd_item.write(login_obj)
                premir_map.update({mirna_id: qid})

                print('Successfull write to item ', qid, 'count:', count, '##' * 20)
                PBB_Core.WDItemEngine.log(
                    'INFO', '{main_data_id},"{exception_type}","{message}",{wd_id},{duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type='',
                            message='success{}'.format(''),
                            wd_id=qid,
                            duration=time.time() - start_time
                        ))

                for x in gene_qids:
                    enc = PBB_Core.WDItemID(value=qid, prop_nr='P688', references=refs)
                    gene_item = PBB_Core.WDItemEngine(wd_item_id=x, data=[enc], append_value=['P688'])
                    gene_item.write(login_obj)
                    print('Encodes written for gene item:', enc)

                progress.update({mirna_id: qid})

            except Exception as e:
                print(e)

                PBB_Core.WDItemEngine.log(
                    'ERROR', '{main_data_id},"{exception_type}","{message}",{wd_id},{duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type=type(e),
                            message=e.__str__(),
                            wd_id='',
                            duration=time.time() - start_time
                        ))

        if count % 10 == 0:
            with open(progress_file_name, 'w') as outfile:
                json.dump(progress, outfile)

    for count, mir in mirbase_data.iterrows():
        start_time = time.time()
        chromosome = mir['chr']
        start = mir['start']
        stop = mir['stop']
        orientation = mir['orientation']

        if mir['type'] == 'miRNA':
            print(mir['info'])
            # gene_qids = set(get_gene(chr=chromosome_map[chromosome], start=start, stop=stop))
            # print(gene_qids)

            mirna_id, alias, name, parent = [x.split('=')[1] for x in mir['info'].split(';')]

            if mirna_id in progress:
                matmir_map.update({mirna_id: progress[mirna_id]})
                continue

            genome_build_qualifier = [
                PBB_Core.WDItemID(value='Q20966585', prop_nr='P659', is_qualifier=True)]
            strand_orientation = 'Q22809680' if orientation == '+' else 'Q22809711'

            refs = make_refs(mirna_label=name, mirna_id=mirna_id, prop_nr='P2871', database='Q6826947')
            data = [
                PBB_Core.WDItemID(value='Q23838648', prop_nr='P31', references=refs),
                PBB_Core.WDItemID(value='Q15978631', prop_nr='P703', references=refs),
                PBB_Core.WDExternalID(value=mirna_id, prop_nr='P2871', references=refs),
                PBB_Core.WDItemID(value=chromosome_map[chromosome], prop_nr='P1057', references=refs),
                PBB_Core.WDString(value=start, prop_nr='P644', references=refs,
                                  qualifiers=genome_build_qualifier),
                PBB_Core.WDString(value=stop, prop_nr='P645', references=refs,
                                  qualifiers=genome_build_qualifier),
                PBB_Core.WDItemID(value=strand_orientation, prop_nr='P2548', references=refs,
                                  qualifiers=genome_build_qualifier),
                PBB_Core.WDItemID(value=premir_map[parent], prop_nr='P361', references=refs),
                PBB_Core.WDBaseDataType.delete_statement('P279')
            ]

            data.extend(generate_targets(name))

            try:
                av = ['P361', 'P644', 'P645', 'P2548', 'P1057', 'P2871']
                if mirna_id.split('_')[0] in matmir_map:
                    wd_item = PBB_Core.WDItemEngine(wd_item_id=matmir_map[mirna_id.split('_')[0]], domain='microRNAs',
                                                    data=data, append_value=av)
                else:
                    wd_item = PBB_Core.WDItemEngine(item_name='miRNA', domain='microRNAs', data=data,
                                                    append_value=av)

                wd_item.set_label(name)
                wd_item.set_description('human mature microRNA')
                if alias != name:
                    wd_item.set_aliases([alias], append=True)

                qid = wd_item.write(login_obj)
                matmir_map.update({mirna_id: qid})

                print('Successfull write to item ', qid, 'count:', count, '##' * 20)
                PBB_Core.WDItemEngine.log(
                    'INFO', '{main_data_id},"{exception_type}","{message}",{wd_id},{duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type='',
                            message='success{}'.format(''),
                            wd_id=qid,
                            duration=time.time() - start_time
                    ))

                if parent in premir_map:
                    p = PBB_Core.WDItemID(value=qid, prop_nr='P527', references=refs)
                    gene_item = PBB_Core.WDItemEngine(wd_item_id=premir_map[parent], data=[p], append_value=['P527'])
                    gene_item.write(login_obj)

                progress.update({mirna_id: qid})

            except Exception as e:
                print(e)

                PBB_Core.WDItemEngine.log(
                    'ERROR', '{main_data_id},"{exception_type}","{message}",{wd_id},{duration}'
                        .format(
                            main_data_id='{}'.format(mirna_id),
                            exception_type=type(e),
                            message=e.__str__(),
                            wd_id='',
                            duration=time.time() - start_time
                    ))

        if count % 10 == 0:
            with open(progress_file_name, 'w') as outfile:
                json.dump(progress, outfile)

    with open(progress_file_name, 'w') as outfile:
        json.dump(progress, outfile)


if __name__ == '__main__':
    sys.exit(main())
