# This Repo has been moved to [Github](https://github.com/SuLab/GeneWikiCentral) #

## README ##

ProteinBoxBots aim at enriching Wikidata with molecular life science databases (e.g. Entrez, Ensembl, Disease ontology, etc).

This repository contains in-progress bots and scripts for reading and writing data to Wikidata. The core functionality for creating bots has been moved to its own repository located [here](https://github.com/SuLab/WikidataIntegrator). And scheduled bots have been moved [here](https://github.com/SuLab/scheduled-bots).